from django.urls import path
from . import views

app_name = "about"
urlpatterns = [
    path('', views.index, name = 'index'),
    path('resume/', views.resume, name = "resume"),
    path('contact/', views.contact, name = "contact"),
    path('schedule/', views.schedule, name = 'schedule'),
    path('jadwal/', views.scheduleView.get, name = 'jadwal'),
    path('hapus/', views.schedule_delete, name = 'hapus'),
    path('deleteobj/<int:id>', views.delete_objek, name = 'hapuss'),
]