from django import forms
from django.forms import widgets
from .models import ScheduleModel

class CustomDateInput(widgets.TextInput):
    input_type = 'date'

class CustomTimeInput(widgets.TextInput):
    input_type = 'time'

# class ScheduleForm(forms.Form):
#     date_of_schedule = forms.DateField(label = 'Date of Event', widget = CustomDateInput)
#     jam_kegiatan = forms.TimeField(label = "Time of Event ", widget = CustomTimeInput)
#     nama_kegiatan = forms.CharField(label = "Nama Kegiatan ", max_length=50)
#     tempat_kegiatan = forms.CharField(label = "Tempat Kegiatan ", max_length=100)
#     kategori_kegiatan = forms.ChoiceField(choices=[('akademis', 'Akademis'), ('keluarga', 'Keluarga'),
#     ('kepanitiaan', 'Kepanitiaan'), ('lainnya', 'Lainnya')])


class ScheduleForm(forms.ModelForm):
    class Meta:
        model = ScheduleModel
        fields = {'nama','kategori', 'tempat', 'date', 'time'}
        widgets = {
            'date' : CustomDateInput,
            'time' : CustomTimeInput
        }