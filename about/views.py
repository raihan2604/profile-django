from django.shortcuts import render, redirect
from .forms import ScheduleForm
from .models import ScheduleModel
from django.views.generic import TemplateView

def index(request):
    return render(request,'about.html')

def resume(request):
    return render(request, 'resume.html')

def contact(request):
    return render(request, 'contact.html')

class scheduleView(TemplateView):
    template_name = 'jadwal.html'

    def get(request):
        form = ScheduleForm()
        jadwals = ScheduleModel.objects.all()
        args = {'jadwals' : jadwals}
        return render(request, 'jadwal.html', args)

def schedule(request):
    if request.method == 'POST':
        form = ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/schedule/')
    else:
        form = ScheduleForm()
    return render(request, 'schedule.html', {'form':form})

def schedule_delete(request):
    ScheduleModel.objects.all().delete()
    return render(request, 'jadwal.html')

def delete_objek(request, id):
    query = ScheduleModel.objects.get(id = id)
    query.delete()
    return redirect('/jadwal/')